#include <stdio.h>
int main(){
    triangle1(1);
    return 0;
}

void triangle1(int a){
   if(a<=4){
        triangle2(a,a);
   }
}

void triangle2(int n,int temp){
    if(temp>0){
        printf("%d",temp);
        triangle2(n,temp-1);
    }
    else{
        printf("\n");
        triangle1(n+1);
    }
}
